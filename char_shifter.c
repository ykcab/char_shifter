/*
Author: ykcab
Course: CS 50
Licence: MIT
version: v0.1
Release date: August 2017
*/

#include <stdio.h>
#include <cs50.h> //specific to the course
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[]){
    
    //Prompt the user the enter a paraphrase
    printf("Enter your String: ");
    string str = get_string(); //in C library this can be GetString()
    
    //declaring variables
    int l= strlen(str);
    char c;
    //convert the argument value to an int
    int shift = atoi(argv[1]);
    
    if (argc >=1){
     
        for (int j = 0; j < l; j++){
            
            // initializing c
            c= str[j]; 
        
            //making sure c is upercase and or lowercase betwen A>>z /cfr ACSII
            if ( (c>='a' && c<='z') || (c>='A' && c<='Z')) {
                
                if (c >'Z'){
                    str[j] =  ( (int)(str[j] -'a') + shift) %26 +'a';
                }
                else{
                    str[j] =  ( (int)(str[j] -'A') + shift) %26 +'A';
                }
            }
        }
        //display the result
        printf("ciphertext: %s\n", str);
    }
}

/*
Below is the improved verion with argument handling

int main(int argc, string argv[]){
    
    char c;
    //check that there is a key
    if(argc < 2 ){
        printf ("Sorry, please pass argument as follow: ./caesar + key \n");
        return 1;
        }
        else if (argc == 2){
            
            //prompt user input
            printf("Please enter the text to be encrypted here : ");
            //storing 
            string str = get_string();
            int l= strlen(str);
            int shift = atoi(argv[1]);
            
            for (int j = 0; j < l; j++){
                //initialize the character
                c= str[j];
                
                //keep original letter(char c) format
                if ( (c>='a' && c<='z') || (c>='A' && c<='Z')) {
                    
                //pointer to be in alphanumeric placement for both Uper & lower
                    if (c >'Z'){
                    str[j] =  ( (int)(str[j] -'a') + shift) %26 +'a';
                    }
                    else{
                        str[j] =  ( (int)(str[j] -'A') + shift) %26 +'A';
                        }
                    }
                }
                //and Voila!
                printf("ciphertext: %s\n", str);
            }
    
}
*/